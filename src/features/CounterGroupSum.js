import {useSelector} from "react-redux";

export const CounterGroupSum = () => {
    const sum = useSelector(state =>
        state.counter.counterList.reduce((prev, cur) => prev + cur, 0)
    );
    return (
        <div>
            Sum: {sum}
        </div>
    )
}