import {useDispatch, useSelector} from "react-redux";
import {updateCounterList} from "./counterSlice";

export const CounterSizeGenerator = () => {
    const counterList = useSelector(state => state.counter.counterList)
    const dispatch = useDispatch()
    const changeSize = (e) => {
        dispatch(updateCounterList(e.target.value))
    }

    return (
        <div className={"counterSizeGenerator"}>
            size:
            <input
                type="number"
                value={counterList.length || ''}
                onChange={changeSize}
            />
        </div>
    )
}