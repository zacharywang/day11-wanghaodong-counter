import {CounterSizeGenerator} from "./CounterSizeGenerator";
import {CounterGroup} from "./CounterGroup";
import {useState} from "react";
import {CounterGroupSum} from "./CounterGroupSum";

export const MultipleCounter = () => {
    return (
        <div className={"multipleCounter"}>
            <CounterSizeGenerator/>
            <CounterGroupSum/>
            <CounterGroup/>
        </div>
    )
}