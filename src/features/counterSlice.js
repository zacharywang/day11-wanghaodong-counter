import {createSlice, current} from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: []
    },
    reducers: {
        updateCounterList: (state, action) => {
            state.counterList = action.payload
                ? Array.from({length: action.payload}).fill(0)
                : []
        },
        updateCounterValue: (state, action) => {
            const {counterValue, index} = action.payload
            state.counterList = state.counterList.map((item, itemIndex) => {
                return itemIndex === index ? counterValue : item;
            })
        },
    },
})

export default counterSlice.reducer;
export const {updateCounterList, updateCounterValue} = counterSlice.actions;